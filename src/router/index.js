import {createRouter, createWebHistory} from 'vue-router'
import Home from '@/views/Home.vue'
import Register from '@/views/Register.vue'
import Login from '@/views/Login.vue'
import List from '@/views/List.vue'


// Const
export const router = createRouter({
    history: createWebHistory(),
    routes: [
          {
              path: '/',
              name: 'Home',
              component: Home
          },
          {
              path: '/register',
              name: 'Register',
              component: Register
          },
          {
            path: '/login',
            name: 'Login',
            component: Login
          },
          {
            path: '/list',
            name: 'List',
            component: List
          }
    ],
  });



export default router